<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="{{ z_language|default:"de"|escape }}" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="{{ z_language|default:"de"|escape }}" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="{{ z_language|default:"de"|escape }}" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="{{ z_language|default:"de"|escape }}" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>{% block title %}{{ id.title }}{% endblock %} – {{m.config.site.title.value }}</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    <meta name="viewport" content="width=device-width" />

    <!-- IE Fix for HTML5 Tags -->
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    {% include "_html_head.tpl" %}
    {% block html_head_extra %}{% endblock %}
</head>

<body class="{% block page_class %}{% endblock %}">

{% block navbar %}
{% include "_navbar.tpl" %}
{% endblock %}

{% block header %}
<header>
    {% include "_header.tpl" %}
</header>
{% endblock %}

{% block content %}
{% endblock %}

{% block footer %}
<footer>
    {% include "_footer.tpl" %}
</footer>
{% endblock %}
{% include "_html_bottom.tpl" %}
{% block bottom %}
{% endblock %}
</body>
</html>

